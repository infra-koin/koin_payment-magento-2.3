<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Response;

use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;

class RedirectHandler implements HandlerInterface
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory
     */
    protected $orderStatusCollectionFactory;

    /**
     * BankSlipHandler constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $orderStatusCollectionFactory
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory $orderStatusCollectionFactory
    ) {
        $this->orderStatusCollectionFactory = $orderStatusCollectionFactory;
    }

    /**
     * Handles transaction id
     *
     * @param array $handlingSubject
     * @param array $response
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function handle(array $handlingSubject, array $response)
    {
        if (!isset($handlingSubject['payment'])
            || !$handlingSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        /** @var PaymentDataObjectInterface $paymentData */
        $paymentData = $handlingSubject['payment'];
        $transaction = $response['transaction'];

        /** @var $payment \Magento\Sales\Model\Order\Payment */
        $payment = $paymentData->getPayment();

        if (isset($transaction['paymentId'])) {
            $state = $this->getPaymentStatusState($payment);

            if ($this->canSkipOrderProcessing($state)) {
                $payment->getOrder()->setState($state);
                $payment->setSkipOrderProcessing(true);
            }
        }
    }

    protected function canSkipOrderProcessing($state)
    {
        return $state != \Magento\Sales\Model\Order::STATE_PROCESSING;
    }

    protected function getPaymentStatusState($payment)
    {
        $paymentMethod = $payment->getMethodInstance();

        if (!$paymentMethod)
            return false;

        $status = $paymentMethod->getConfigData('order_status');

        if (!$status)
            return false;

        $state = $this->getStatusState($status);

        if (!$state)
            return false;

        return $state;
    }

    /**
     * Returns a state code from a status code
     *
     * @param $status
     * @return string
     */
    protected function getStatusState($status)
    {
        $statuses = $this->orderStatusCollectionFactory
            ->create()
            ->joinStates()
            ->addFieldToFilter('main_table.status', $status);

        if($statuses->getSize())
            return $statuses->getFirstItem()->getState();

        return '';
    }
}
