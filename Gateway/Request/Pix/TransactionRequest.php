<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Request\Pix;

use Koin\Payment\Helper\Data;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Directory\Helper\Data as DirectoryData;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;

class TransactionRequest implements BuilderInterface
{
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CustomerSession $customerSession
     */
    protected $customerSession;

    /**
     * @var ManagerInterface
     */
    protected $eventManager;

    /**
     * @var DateTime
     */
    protected $dateTime;

    /**
     * @param ManagerInterface $eventManager
     * @param Data $helper
     * @param DateTime $date
     * @param ConfigInterface $config
     * @param CustomerSession $customerSession
     * @param DateTime $dateTime
     */
    public function __construct(
        ManagerInterface $eventManager,
        Data             $helper,
        DateTime         $date,
        ConfigInterface  $config,
        CustomerSession  $customerSession,
        DateTime         $dateTime
    ) {
        $this->eventManager = $eventManager;
        $this->helper = $helper;
        $this->date = $date;
        $this->config = $config;
        $this->customerSession = $customerSession;
        $this->dateTime = $dateTime;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function build(array $buildSubject)
    {
        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        /** @var \Magento\Sales\Model\Order\Payment\Interceptor $payment */
        $payment = $buildSubject['payment']->getPayment();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $payment->getOrder();

        $request = new \stdClass();
        $request->store = $this->getStoreData();
        $request->transaction = $this->getTransaction($order, $buildSubject['amount']);
        $request->payment_method = $this->getPaymentMethod($payment->getMethod());
        $request->payer = $this->getPayerData($order);
        $request->country_code = $this->helper->getDefaultCountryCode();
        $request->descriptor = __('Order %1 on %2', $order->getRealOrderId(), $this->helper->getStoreName());
        $request->notification_url = [$this->helper->getPaymentsNotificationUrl($order)];

        return ['request' => $request];
    }

    /**
     * @return \stdClass
     */
    protected function getStoreData()
    {
        $store = new \stdClass();
        $store->code = $this->helper->getPaymentsConfig('store_code') ?: Data::DEFAULT_MERCHANT_NAME;
        return $store;
    }

    /**
     * @param $paymentMethod
     * @return \stdClass
     */
    protected function getPaymentMethod($paymentMethod)
    {
        $payment = new \stdClass();

        $methodCode = $this->helper->getConfig('code', $paymentMethod);
        $payment->code = $methodCode ?: Data::DEFAULT_PAYMENTS_METHOD_CODE;
        $payment->expiration_date = $this->getExpirationDate();
        if ($this->helper->getConfig('use_custom_pix_key', 'koin_pix')) {
            $customPixKey = trim($this->helper->getConfig('custom_pix_key', 'koin_pix'));
            if ($customPixKey) {
                $payment->payment_key = $customPixKey;
            }
        }

        return $payment;
    }

    /**
     * @return string
     */
    protected function getExpirationDate()
    {
        $expirationTime = (int) $this->helper->getConfig('expiration_time', 'koin_pix');
        $expirationTime = $expirationTime > 0 ? $expirationTime : \Koin\Payment\Helper\Order::DEFAULT_EXPIRATION_TIME;
        $minutes = "+{$expirationTime} minutes";
        $timeStamp = $this->dateTime->timestamp($minutes);
        return $this->dateTime->gmtDate('Y-m-d\TH:i:s', $timeStamp) . '.000Z';
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param float $amount
     * @return \stdClass
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTransaction($order, $amount)
    {
        $transaction = new \stdClass();
        $transaction->amount = new \stdClass();
        $transaction->amount->currency_code = $order->getBaseCurrencyCode() ?: $this->helper->getStoreCurrencyCode();
        $transaction->amount->value = $amount;
        $transaction->reference_id = $order->getRealOrderId();
        $transaction->business_id = $this->helper->getPaymentsConfig('business_id');
        $transaction->account = $this->helper->getPaymentsConfig('account_number');

        return $transaction;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     */
    protected function getPayerData($order)
    {
        $customerTaxVat = $order->getPayment()->getAdditionalInformation('koin_customer_taxvat');
        if (!$customerTaxVat) {
            $customerTaxVat = $order->getCustomerTaxvat();
        }
        $customerTaxVat = $this->helper->clearNumber($customerTaxVat);

        $payerData = new \stdClass();
        $payerData->first_name = $order->getCustomerFirstname();
        $payerData->last_name = $order->getCustomerLastname();
        $payerData->email = $order->getCustomerEmail();
        $payerData->document = $this->getDocument($customerTaxVat);

        $phoneNumber = $this->helper->formatPhoneNumber($order->getBillingAddress()->getTelephone());
        $payerData->phone = new \stdClass();
        $payerData->phone->area = substr($phoneNumber, 0, 2);
        $payerData->phone->number = substr($phoneNumber, 2, 9);

        return $payerData;
    }

    /**
     * @param string $customerTaxVat
     * @return \stdClass
     */
    public function getDocument($customerTaxVat)
    {
        $document = new \stdClass();
        $document->type = 'cpf';
        $document->number = $customerTaxVat;

        if (strlen($customerTaxVat) == 14 || $this->helper->isCompanyCustomer()) {
            $document->type = 'cnpj';
        }
        //It's possible to add passaport in future
        return $document;
    }
}
