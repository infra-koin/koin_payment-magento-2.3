<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Request\Redirect;

use Koin\Payment\Helper\Data;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Gateway\Data\PaymentDataObjectInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Catalog\Model\Product\Type as ProductType;

class TransactionRequest implements BuilderInterface
{
    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var CustomerSession $customerSession
     */
    protected $customerSession;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @param Data $helper
     * @param DateTime $date
     * @param ConfigInterface $config
     * @param CustomerSession $customerSession
     */
    public function __construct(
        ManagerInterface $eventManager,
        Data $helper,
        DateTime $date,
        ConfigInterface $config,
        CustomerSession $customerSession
    ) {
        $this->eventManager = $eventManager;
        $this->helper = $helper;
        $this->date = $date;
        $this->config = $config;
        $this->customerSession = $customerSession;
    }

    /**
     * Builds ENV request
     *
     * @param array $buildSubject
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function build(array $buildSubject)
    {
        if (!isset($buildSubject['payment'])
            || !$buildSubject['payment'] instanceof PaymentDataObjectInterface
        ) {
            throw new \InvalidArgumentException('Payment data object should be provided');
        }

        /** @var \Magento\Sales\Model\Order\Payment\Interceptor $payment */
        $payment = $buildSubject['payment']->getPayment();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $payment->getOrder();

        $request = new \stdClass();

        $request->totalCartValue = $buildSubject['amount'];
        $request->value = $buildSubject['amount'];
        $request->deviceFingerprint = $this->customerSession->getSessionId();

        $request->orderId = $order->getRealOrderId();
        $request->ipAddress = $this->helper->getCurrentIpAddress();
        $request->merchantName = $this->helper->getStoreName() ?: Data::DEFAULT_MERCHANT_NAME;
        $request->url = $this->helper->getUrl('sales/order/view', ['order_id' => $order->getId()]);
        $request->reference = $order->getIncrementId();
        $request->transactionId = $order->getIncrementId();
        $request->installments = 1;
        $request->paymentId = $order->getRealOrderId();
        $request->shopperInteraction = 'ecommerce';
        $request->paymentMethod = 'BoletoParceladoKoin';
        $request->currency = $this->helper->getStoreCurrencyCode();
        $request->callbackUrl = $this->helper->getCallbackUrl();
        $request->miniCart = $this->getMiniCart($order);
        $request->returnUrl = $this->helper->getReturnUrl($order->getIncrementId());

        return ['request' => $request];
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return \stdClass
     */
    protected function getMiniCart($order)
    {
        $shippingAddress = $order->getShippingAddress();
        if (!$shippingAddress) {
            $shippingAddress = $order->getBillingAddress();
        }
        $miniCart = new \stdClass();
        $miniCart->buyer = $this->getBuyerData($order);
        $miniCart->shippingAddress = $this->getAddressData($shippingAddress);
        $miniCart->billingAddress = $this->getAddressData($order->getBillingAddress());
        $miniCart->items = $this->getItemsData($order->getAllItems());
        $miniCart->shippingValue = $order->getShippingAmount();
        $miniCart->taxValue = $order->getTaxAmount();
        return $miniCart;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     */
    protected function getBuyerData($order)
    {
        $buyerData = new \stdClass();
        $buyerData->id = $order->getCustomerId() ?: $order->getQuoteId();
        $buyerData->firstName = $order->getCustomerFirstname();
        $buyerData->lastName = $order->getCustomerLastname();
        $buyerData->document = $order->getPayment()->getAdditionalInformation('koin_customer_taxvat');
        $buyerData->documentType = 'cpf';
        $buyerData->isCorporate = false;
        $buyerData->email = $order->getCustomerEmail();
        $buyerData->phone = $this->helper->formatPhoneNumber($order->getBillingAddress()->getTelephone());
        return $buyerData;
    }

    /**
     * @param \Magento\Sales\Model\Order\Address $address
     * @return \stdClass
     */
    protected function getAddressData($address)
    {
        $fullStreet = $address->getStreet();
        $street = $this->helper->getConfig('street', 'address', 'koin');
        $streetNumber = $this->helper->getConfig('number', 'address', 'koin');
        $neighborhood = $this->helper->getConfig('district', 'address', 'koin');
        $complement = $this->helper->getConfig('complement', 'address', 'koin');

        $addressData = new \stdClass();
        $addressData->country = 'BRA';
        $addressData->street = $fullStreet[$street] ?? 'N/A';
        $addressData->number = $fullStreet[$streetNumber] ?? 'N/A';
        $addressData->complement = $fullStreet[$complement] ?? 'N/A';
        $addressData->neighborhood = $fullStreet[$neighborhood] ?? 'N/A';
        $addressData->postalCode = $this->helper->digits($address->getPostcode());
        $addressData->city = $address->getCity();
        $addressData->state = $address->getRegionCode();

        return $addressData;
    }

    /**
     * @param array $quoteItems
     */
    protected function getItemsData($quoteItems)
    {
        $items = [];
        /** @var \Magento\Sales\Api\Data\OrderItemInterface $quoteItem */
        foreach ($quoteItems as $quoteItem) {
            if ($quoteItem->getParentItemId() || $quoteItem->getParentItem() || $quoteItem->getPrice() == 0) {
                continue;
            }

            $discountAmount = ((float) $quoteItem->getDiscountAmount() > 0)
                ? (float) $quoteItem->getDiscountAmount() * -1
                : (float) $quoteItem->getDiscountAmount();

            $item = new \stdClass();
            $item->id = $quoteItem->getProductId();
            $item->name = $quoteItem->getName();
            $item->price = $quoteItem->getPrice();
            $item->quantity = $quoteItem->getQtyOrdered();
            $item->discount = $discountAmount;
            $item->deliveryType = $quoteItem->getProductType() !== ProductType::TYPE_VIRTUAL ? 'Normal' : 'Virtual';
            $item->taxValue = $quoteItem->getTaxAmount();
            $item->taxRate = $quoteItem->getTaxPercent();

            $this->eventManager->dispatch('koin_payment_get_item', ['item' => &$item, 'quote_item' => $quoteItem]);

            $items[] = $item;
        }

        return $items;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return null
     */
    protected function getCustomerTaxvat($order)
    {
        $customerTaxvat =  $order->getBillingAddress()->getVatId();
        if (!$customerTaxvat) {
            $customerTaxvat = $order->getCustomerTaxvat();
        }

        return $customerTaxvat;
    }
}
