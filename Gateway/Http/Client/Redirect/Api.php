<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Http\Client\Redirect;

use Koin\Payment\Gateway\Http\Client\Redirect\Api\Create;
use Koin\Payment\Helper\Data;

class Api
{
    const STATUS_UNDEFINED = 'undefined';
    const STATUS_APPROVED = 'approved';
    const STATUS_DENIED = 'denied';

    const STATUS_REASON_EMAIL_VALIDATION = 'EmailValidation';
    const STATUS_REASON_PROVIDER_REVIEW = 'ProviderReview';
    const STATUS_REASON_FIRST_PAYMENT = 'FirstPayment';

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Create
     */
    private $create;

    /**
     * @param Data $helper
     * @param Create $create
     */
    public function __construct(
        Data $helper,
        Create $create
    ) {
        $this->helper = $helper;
        $this->create = $create;
    }

    public function create()
    {
        return $this->create;
    }

    /**
     * @param $request
     * @param string $name
     */
    public function logRequest($request, $name = 'koin')
    {
        $this->helper->log('Request', $name);
        $this->helper->log($request, $name);
    }

    /**
     * @param $response
     * @param string $name
     */
    public function logResponse($response, $name = 'koin')
    {
        $this->helper->log('RESPONSE', $name);
        $this->helper->log($response, $name);
    }

    /**
     * @param $request
     * @param $response
     * @param $statusCode
     * @return void
     */
    public function saveRequest($request, $response, $statusCode)
    {
        $method = \Koin\Payment\Model\Ui\Redirect\ConfigProvider::CODE;
        $this->helper->saveRequest($request, $response, $statusCode, $method);
    }
}
