<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Http\Client\Payments\Api;

use Koin\Payment\Gateway\Http\Client;
use Laminas\Http\Request;

class Refund extends Client
{
    /**
     * refund payment action on Koin API
     * @param $data
     * @param $orderId
     * @return array
     */
    public function refundPayment($data, $orderId)
    {
        $path = $this->getEndpointPath('payments/refund', $orderId);
        $api = $this->getApi($path);

        $api->setMethod(Request::METHOD_PUT);
        $api->setRawBody($this->json->serialize($data));

        $response = $api->send();
        $content = $response->getContent();
        if ($content && $response->getStatusCode() != 204) {
            $content = $this->json->unserialize($content);
        }

        return [
            'status' => $response->getStatusCode(),
            'response' => $content
        ];
    }
}
