<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Http\Client\Payments\Api;

use Koin\Payment\Gateway\Http\Client;
use Koin\Payment\Gateway\Http\Client\Payments\Api;
use Laminas\Http\Request;

class Create extends Client
{
    /**
     * initi payment action on Koin API
     * @param $data
     * @return array
     */
    public function initPayment($data)
    {
        $path = $this->getEndpointPath('payments/create');
        $api = $this->getApi($path);

        $api->setMethod(Request::METHOD_POST);
        $api->setRawBody($this->json->serialize($data));

        $response = $api->send();
        $content = $response->getContent();
        if ($content && $response->getStatusCode() != 204) {
            $content = $this->json->unserialize($content);
        }

        //Retry a couple of times because sometimes pix takes longer than usual and return Opened while it is process yet
        if (isset($content['status']['type']) && $content['status']['type'] == Api::STATUS_OPENED) {
            if ($response->getStatusCode() == 200) {
                sleep(40);
                $response = $api->send();
                $content = $response->getContent();
                if ($content && $response->getStatusCode() != 204) {
                    $content = $this->json->unserialize($content);
                }
            }
        }

        return [
            'status' => $response->getStatusCode(),
            'response' => $content
        ];
    }
}
