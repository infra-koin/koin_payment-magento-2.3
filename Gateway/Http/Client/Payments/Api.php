<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Http\Client\Payments;

use Koin\Payment\Gateway\Http\Client\Payments\Api\Create;
use Koin\Payment\Gateway\Http\Client\Payments\Api\Refund;
use Koin\Payment\Helper\Data;

class Api
{
    const STATUS_OPENED = 'Opened';
    const STATUS_PUBLISHED = 'Published';
    const STATUS_COLLECTED = 'Collected';
    const STATUS_REFUNDED = 'Refunded';
    const STATUS_CANCELLED = 'Cancelled';
    const STATUS_VOIDED = 'Voided';
    const STATUS_FAILED = 'Failed';

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Create
     */
    private $create;

    /**
     * @var Refund
     */
    private $refund;

    /**
     * @param Data $helper
     * @param Transaction $transaction
     * @param Refund $refund
     */
    public function __construct(
        Data $helper,
        Create $create,
        Refund $refund
    ) {
        $this->helper = $helper;
        $this->create = $create;
        $this->refund = $refund;
    }

    public function create()
    {
        return $this->create;
    }

    public function refund()
    {
        return $this->refund;
    }

    /**
     * @param $request
     * @param string $name
     */
    public function logRequest($request, $name = 'koin-pix')
    {
        $this->helper->log('Request', $name);
        $this->helper->log($request, $name);
    }

    /**
     * @param $response
     * @param string $name
     */
    public function logResponse($response, $name = 'koin-pix')
    {
        $this->helper->log('RESPONSE', $name);
        $this->helper->log($response, $name);
    }

    /**
     * @param $request
     * @param $response
     * @param $statusCode
     * @return void
     */
    public function saveRequest(
        $request,
        $response,
        $statusCode,
        $method = \Koin\Payment\Model\Ui\Pix\ConfigProvider::CODE
    ) {
        $this->helper->saveRequest($request, $response, $statusCode, $method);
    }
}
