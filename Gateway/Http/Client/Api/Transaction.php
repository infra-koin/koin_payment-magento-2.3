<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Http\Client\Api;

use Zend\Http\Request;

class Transaction extends Client
{
    /**
     * initi payment action on Koin API
     * @param $data
     * @return array
     */
    public function initPayment($data)
    {
        $path = $this->getEndpointPath('init_payment');
        $api = $this->getApi($path);

        $api->setMethod(Request::METHOD_POST);
        $api->setRawBody($this->json->serialize($data));

        $response = $api->send();

        return [
            'status' => $response->getStatusCode(),
            'response' => $this->json->unserialize($response->getContent())
        ];
    }
}
