<?php
/**
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 */

namespace Koin\Payment\Gateway\Http\Client\Api;

use Magento\Framework\Encryption\EncryptorInterface;
use Zend\Http\Client as HttpClient;
use Magento\Framework\Serialize\Serializer\Json;
use Koin\Payment\Helper\Data;

class Client
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var HttpClient
     */
    protected $api;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var string
     */
    protected $token;


    /**
     * @param Data $helper
     * @param EncryptorInterface $encryptor
     * @param Json $json
     */
    public function __construct(
        Data $helper,
        EncryptorInterface $encryptor,
        Json $json
    ) {
        $this->helper = $helper;
        $this->encryptor = $encryptor;
        $this->json = $json;
    }

    /**
     * @param null $token
     */
    public function setToken($token = null)
    {
        $this->token = $token ?: $this->helper->getGeneralConfig('token');
    }

    /**
     * @return string
     */
    public function getToken()
    {
        if (!$this->token) {
            $this->setToken();
        }

        return $this->token;
    }

    /**
     * @return array
     */
    protected function getDefaultHeaders()
    {
        $appKey = $this->helper->getGeneralConfig('app_key');
        $appToken = $this->helper->getGeneralConfig('app_token');
        $defaultHeader = [
            'Content-Type' => 'application/json',
            'X-API-AppKey' => $appKey,
            'X-API-AppToken' => $appToken
        ];

        return $defaultHeader;
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [
            'timeout' => 30
        ];
    }

    /**
     * @param string $endpoint
     * @param string $id
     * @return string|string[] $path
     */
    protected function getEndpointPath($endpoint, $id = null)
    {
        $fullEndpoint = $this->helper->getEndpointConfig($endpoint);
        $path = str_replace(
            ['{{id}}'],
            [$id],
            $fullEndpoint
        );
        return $path;
    }

    /**
     * @return HttpClient
     */
    public function getApi($path)
    {
        $uri = $this->helper->getGeneralConfig('uri');
        if ($this->helper->getGeneralConfig('use_sandbox')) {
            $uri = $this->helper->getGeneralConfig('uri_sandbox');
        }

        $this->api = new HttpClient(
            $uri . $path,
            $this->getDefaultOptions()
        );

        $this->api->setHeaders($this->getDefaultHeaders());
        $this->api->setEncType('application/json');

        return $this->api;
    }
}
