<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Http\Client\Risk\Api;

use Koin\Payment\Gateway\Http\Client;
use Laminas\Http\Request;

class Evaluation extends Client
{
    /**
     * initi payment action on Koin API
     * @param $data
     * @return array
     */
    public function sendData($data)
    {
        $path = $this->getEndpointPath('risk/evaluations');
        $api = $this->getApi($path);

        $api->setMethod(Request::METHOD_POST);
        $api->setRawBody($this->json->serialize($data));

        $response = $api->send();
        $content = $response->getContent();
        if ($content && $response->getStatusCode() != 204) {
            $content = $this->json->unserialize($content);
        }

        return [
            'status' => $response->getStatusCode(),
            'response' => $content
        ];
    }

    /**
     * initi payment action on Koin API
     * @param $data
     * @return array
     */
    public function getStatus($evaluationId)
    {
        $path = $this->getEndpointPath('risk/get_status', null, $evaluationId);
        $api = $this->getApi($path);

        $api->setMethod(Request::METHOD_GET);

        $response = $api->send();
        $content = $response->getContent();
        if ($content && $response->getStatusCode() != 204) {
            $content = $this->json->unserialize($content);
        }

        return [
            'status' => $response->getStatusCode(),
            'response' => $content
        ];
    }

    /**
     * initi payment action on Koin API
     * @param $data
     * @return array
     */
    public function cancel($evaluationId)
    {
        $path = $this->getEndpointPath('risk/cancel', null, $evaluationId);
        $api = $this->getApi($path);

        $api->setMethod(Request::METHOD_DELETE);

        $response = $api->send();
        $content = $response->getContent();
        if ($content && $response->getStatusCode() != 204) {
            $content = $this->json->unserialize($content);
        }

        return [
            'status' => $response->getStatusCode(),
            'response' => $content
        ];
    }

    /**
     * initi payment action on Koin API
     * @param $data
     * @return array
     */
    public function notification($evaluationId, $data)
    {
        $path = $this->getEndpointPath('risk/notifications', null, $evaluationId);
        $api = $this->getApi($path);

        $api->setMethod(Request::METHOD_PATCH);
        $api->setRawBody($this->json->serialize($data));

        $response = $api->send();
        $content = $response->getContent();
        if ($content && $response->getStatusCode() != 204) {
            $content = $this->json->unserialize($content);
        }

        return [
            'status' => $response->getStatusCode(),
            'response' => $content
        ];
    }
}
