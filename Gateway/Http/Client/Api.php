<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Gateway\Http\Client;

use Koin\Payment\Gateway\Http\Client\Api\Transaction;
use Koin\Payment\Helper\Data;

class Api
{
    const STATUS_UNDEFINED = 'undefined';
    const STATUS_APPROVED = 'approved';
    const STATUS_DENIED = 'denied';

    const STATUS_REASON_EMAIL_VALIDATION = 'EmailValidation';
    const STATUS_REASON_PROVIDER_REVIEW = 'ProviderReview';
    const STATUS_REASON_FIRST_PAYMENT = 'FirstPayment';

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @param Data $helper
     * @param Transaction $transaction
     */
    public function __construct(
        Data $helper,
        Transaction $transaction
    ) {
        $this->helper = $helper;
        $this->transaction = $transaction;
    }

    public function transaction()
    {
        return $this->transaction;
    }

    /**
     * @param $request
     * @param string $name
     */
    public function logRequest($request, $name = 'koin')
    {
        $this->helper->log('Request', $name);
        $this->helper->log($request, $name);
    }

    /**
     * @param $response
     * @param string $name
     */
    public function logResponse($response, $name = 'koin')
    {
        $this->helper->log('RESPONSE', $name);
        $this->helper->log($response, $name);
    }
}
