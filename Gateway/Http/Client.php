<?php
/**
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 */

namespace Koin\Payment\Gateway\Http;

use Magento\Framework\Encryption\EncryptorInterface;
use Laminas\Http\Client as HttpClient;
use Magento\Framework\Serialize\Serializer\Json;
use Koin\Payment\Helper\Data;
use Firebase\JWT\JWT;

class Client
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var HttpClient
     */
    protected $api;

    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @var Json
     */
    protected $json;

    /**
     * @var string
     */
    protected $token;


    /**
     * @param Data $helper
     * @param EncryptorInterface $encryptor
     * @param Json $json
     */
    public function __construct(
        Data $helper,
        EncryptorInterface $encryptor,
        Json $json
    ) {
        $this->helper = $helper;
        $this->encryptor = $encryptor;
        $this->json = $json;
    }

    /**
     * @param null $token
     */
    public function setToken($token = null)
    {
        $this->token = $token ?: $this->helper->getGeneralConfig('token');
    }

    /**
     * @return string
     */
    public function getToken()
    {
        if (!$this->token) {
            $this->setToken();
        }

        return $this->token;
    }

    /**
     * @return array
     */
    protected function getDefaultHeaders()
    {
        $apiKey = $this->helper->getPaymentsConfig('api_key');
        $apiSecret = $this->helper->getPaymentsConfig('api_secret');
        $apiCommerce = $this->helper->getPaymentsConfig('commerce');

        $payload = [
            'aud' => 'api-koin',
            'iss' => $apiCommerce,
            'iat' => time(),
            'exp' => time() + 600,
        ];

        $jwt = JWT::encode($payload, $apiSecret, 'HS512', $apiKey);
        return [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $jwt
        ];
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [
            'timeout' => 30
        ];
    }

    /**
     * @param string $endpoint
     * @param string $orderId
     * @param string $evaluationId
     * @return string $path
     */
    public function getEndpointPath($endpoint, $orderId = null, $evaluationId = null)
    {
        $fullEndpoint = $this->helper->getEndpointConfig($endpoint);
        return str_replace(
            ['{order_id}', '{evaluation_id}'],
            [$orderId, $evaluationId],
            $fullEndpoint
        );
    }

    /**
     * @return HttpClient
     */
    public function getApi($path)
    {
        $uri = $this->helper->getPaymentsConfig('uri_payments');
        if ($this->helper->getPaymentsConfig('use_sandbox')) {
            $uri = $this->helper->getPaymentsConfig('uri_payments_sandbox');
        }

        $this->api = new HttpClient(
            $uri . $path,
            $this->getDefaultOptions()
        );

        $this->api->setHeaders($this->getDefaultHeaders());
        $this->api->setEncType('application/json');

        return $this->api;
    }
}
