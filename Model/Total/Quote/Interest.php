<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Model\Total\Quote;

use Koin\Payment\Helper\Installments;
use Magento\Checkout\Model\Session;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;

class Interest extends AbstractTotal
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var Installments
     */
    private $helperInstallments;

    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * PayInterest constructor.
     *
     * @param Session $checkoutSession
     * @param Installments $helperInstallments
     * @param CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        Session $checkoutSession,
        Installments $helperInstallments,
        CartRepositoryInterface $quoteRepository
    ) {
        $this->setCode('koin_interest');
        $this->checkoutSession = $checkoutSession;
        $this->helperInstallments = $helperInstallments;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Return selected installments
     *
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getInstallments()
    {
        $installments = 0;

        /** @var \Magento\Quote\Model\Quote $quote */
        //Prepared for Koin transparent
//        $quoteId = $this->checkoutSession->getQuoteId();
//        if ($quoteId) {
//            $quote = $this->quoteRepository->get($quoteId);
//            if ($quote->getPayment()->getMethod() == ConfigProvider::CODE) {
//                $installments = (int)$this->checkoutSession->getData('koin_installments');
//            }
//        }

        return $installments;
    }

    /**
     * Calculate interest rate amount
     *
     * @return int|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Exception
     */
    protected function getInterestAmount()
    {
        $installments = $this->getInstallments();
        if ($installments > 1) {
            /** @var \Magento\Quote\Model\Quote $quote */
            $quoteId = $this->checkoutSession->getQuoteId();
            if ($quoteId) {
                $quote = $this->quoteRepository->get($quoteId);
                $grandTotal = $quote->getGrandTotal() - $quote->getKoinInterestAmount();
                $installmentsPrice = $this->helperInstallments->getInstallmentPrice($grandTotal, $installments);
                $totalWithInterest = $installmentsPrice * $installments;
                if ($totalWithInterest > $grandTotal) {
                    return $totalWithInterest - $grandTotal;
                }
            }
        }

        return 0;
    }

    /**
     * Collect address discount amount
     *
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        $items = $shippingAssignment->getItems();
        if (!count($items)) {
            return $this;
        }

        $interest = $this->getInterestAmount();

        $quote->setKoinInterestAmount($interest);
        $quote->setBaseKoinInterestAmount($interest);

        $total->setKoinInterestDescription($this->getCode());
        $total->setKoinInterestAmount($interest);
        $total->setBaseKoinInterestAmount($interest);

        $total->addTotalAmount($this->getCode(), $interest);
        $total->addBaseTotalAmount($this->getCode(), $interest);

        return $this;
    }

    /**
     * @param Quote $quote
     * @param Total $total
     *
     * @return array
     */
    public function fetch(Quote $quote, Total $total)
    {
        $result = null;
        $amount = $total->getKoinInterestAmount();

        if ($amount) {
            $result = [
                'code' => $this->getCode(),
                'title' => __('Interest Rate'),
                'value' => $amount
            ];
        }

        return $result;
    }
}
