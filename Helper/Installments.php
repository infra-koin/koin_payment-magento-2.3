<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Installments data helper, prepared for Koin Transparent
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Installments extends AbstractHelper
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @param Context $context
     * @param PriceCurrencyInterface $priceCurrency
     * @param Data $helper
     */
    public function __construct(
        Context $context,
        PriceCurrencyInterface $priceCurrency,
        Data $helper
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function getAllInstallments($price = null)
    {
        return [
            ['value' => 1, 'text' => __('1x of %1 (without interest)', $this->priceCurrency->format($price, false))]
        ];
    }

    /**
     * @param float $price
     * @param int $installment
     * @return float
     * @throws Exception
     */
    public function getInstallmentPrice($price, $installments)
    {
        return (float) $price / (int) $installments;
    }
}
