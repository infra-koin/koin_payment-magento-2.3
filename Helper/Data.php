<?php
/**
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 */

namespace Koin\Payment\Helper;

use Koin\Payment\Logger\Logger;
use Koin\Payment\Api\RequestRepositoryInterface;
use Koin\Payment\Model\RequestFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Directory\Helper\Data as DirectoryData;
use Magento\Framework\App\Config\Initial;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\LayoutFactory;
use Magento\Payment\Model\Config;
use Magento\Payment\Model\Method\Factory;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\App\Emulation;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Class Data
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Data extends \Magento\Payment\Helper\Data
{
    const ROUND_FACTOR = 100;
    const DEFAULT_USAGE = 'single_use';
    const DEFAULT_PAYMENTS_METHOD_CODE = 'PIX';
    const DEFAULT_MERCHANT_NAME = 'adobe';
    const DEFAULT_INTEREST_MODE = 'interest_free';
    const MEDIA_FOLDER = 'koin';
    const FINGERPRINT_URL = 'https://securegtm.despegar.com/risk/fingerprint/statics/track-min.js';

    /** @var \Koin\Payment\Logger\Logger */
    protected $logger;

    /** @var OrderInterface  */
    protected $order;

    /** @var RequestRepositoryInterface  */
    protected $requestRepository;

    /** @var RequestFactory  */
    protected $requestFactory;

    /** @var WriterInterface */
    private $configWriter;

    /** @var Json */
    private $json;

    /** @var StoreManagerInterface */
    private $storeManager;

    /** @var RemoteAddress */
    private $remoteAddress;

    /** @var CategoryRepositoryInterface  */
    protected $categoryRepository;

    /** @var CustomerSession  */
    protected $customerSession;

    /**
     * @var DirectoryData
     */
    protected  $helperDirectory;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param LayoutFactory $layoutFactory
     * @param Factory $paymentMethodFactory
     * @param Emulation $appEmulation
     * @param Config $paymentConfig
     * @param Initial $initialConfig
     * @param Logger $logger
     * @param WriterInterface $configWriter
     * @param Json $json
     * @param StoreManagerInterface $storeManager
     * @param RemoteAddress $remoteAddress
     * @param CustomerSession $customerSession
     * @param CategoryRepositoryInterface $categoryRepository
     * @param RequestRepositoryInterface $requestRepository
     * @param RequestFactory $requestFactory
     * @param OrderInterface $order
     * @param DirectoryData $helperDirectory
     */
    public function __construct(
        Context $context,
        LayoutFactory $layoutFactory,
        Factory $paymentMethodFactory,
        Emulation $appEmulation,
        Config $paymentConfig,
        Initial $initialConfig,
        Logger $logger,
        WriterInterface $configWriter,
        Json $json,
        StoreManagerInterface $storeManager,
        RemoteAddress $remoteAddress,
        CustomerSession $customerSession,
        CategoryRepositoryInterface $categoryRepository,
        RequestRepositoryInterface $requestRepository,
        RequestFactory $requestFactory,
        OrderInterface $order,
        DirectoryData $helperDirectory
    ) {
        parent::__construct($context, $layoutFactory, $paymentMethodFactory, $appEmulation, $paymentConfig, $initialConfig);

        $this->logger = $logger;
        $this->configWriter = $configWriter;
        $this->json = $json;
        $this->storeManager = $storeManager;
        $this->remoteAddress = $remoteAddress;
        $this->customerSession = $customerSession;
        $this->categoryRepository = $categoryRepository;
        $this->requestRepository = $requestRepository;
        $this->requestFactory = $requestFactory;
        $this->order = $order;
        $this->helperDirectory = $helperDirectory;
    }

    public function getAllowedMethods()
    {
        return [
            \Koin\Payment\Model\Ui\Redirect\ConfigProvider::CODE,
            \Koin\Payment\Model\Ui\Pix\ConfigProvider::CODE,
        ];
    }

    public function getFinalStates()
    {
        return [
            Order::STATE_CANCELED,
            Order::STATE_CLOSED,
            Order::STATE_COMPLETE
        ];
    }

    public function isActive()
    {
        return $this->getConfig('active');
    }

    /**
     * Log custom message using Koin logger instance
     *
     * @param $message
     * @param string $name
     * @param void
     */
    public function log($message, $name = 'koin')
    {
        if ($this->getGeneralConfig('debug')) {
            if (!is_string($message)) {
                $message = $this->json->serialize($message);
            }

            $this->logger->setName($name);
            $this->logger->debug($message);
        }
    }

    /**
     * @param $message
     * @return bool|string
     */
    public function jsonEncode($message)
    {
        return $this->json->serialize($message);
    }

    /**
     * @param $message
     * @return bool|string
     */
    public function jsonDecode($message)
    {
        return $this->json->unserialize($message);
    }

    /**
     * Log custom message using Koin logger instance
     *
     * @param $message
     * @param string $name
     * @param void
     */
    public function saveRequest($request, $response, $statusCode, $method = 'koin')
    {
        if ($this->getGeneralConfig('debug')) {
            try {
                if (!is_string($request)) {
                    $request = $this->json->serialize($request);
                }
                if (!is_string($response)) {
                    $response = $this->json->serialize($response);
                }

                $requestModel = $this->requestFactory->create();
                $requestModel->setRequest($request);
                $requestModel->setResponse($response);
                $requestModel->setMethod($method);
                $requestModel->setStatusCode($statusCode);

                $this->requestRepository->save($requestModel);
            } catch (\Exception $e) {
                $this->log($e->getMessage());
            }
        }
    }

    /**
     * @param $config
     * @param string $group
     * @param string $section
     * @return mixed
     */
    public function getConfig($config, $group = 'koin_redirect', $section = 'payment')
    {
        return $this->scopeConfig->getValue(
            $section . '/' . $group . '/' . $config,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $config
     * @return mixed
     */
    public function getAntifraudConfig($config)
    {
        return $this->getConfig($config, 'fraud_analysis', 'koin');
    }

    /**
     * @param string $value
     * @param string $config
     * @param string $group
     * @param string $section
     */
    public function saveConfig($value, $config, $group = 'general', $section = 'koin')
    {
        $this->configWriter->save(
            $section . '/' . $group . '/' . $config,
            $value
        );
    }

    /**
     * @param $config
     * @param string $group
     * @param string $section
     * @return mixed
     */
    public function getGeneralConfig($config)
    {
        return $this->getConfig($config, 'general', 'koin');
    }

    /**
     * @param $config
     * @param string $group
     * @param string $section
     * @return mixed
     */
    public function getPaymentsConfig($config)
    {
        return $this->getConfig($config, 'payments', 'koin');
    }

    /**
     * @param string $order
     * @return string
     */
    public function getCallbackUrl()
    {
        return $this->storeManager->getStore()->getUrl(
            'koin/callback/',
            [
                '_query' => ['hash' => sha1($this->getGeneralConfig('app_key'))],
                '_secure' => true
            ]
        );
    }

    /**
     * @param string $type
     * @return string
     */
    public function getPaymentsNotificationUrl($order)
    {
        $orderId = $order->getStoreId() ?: $this->storeManager->getDefaultStoreView()->getId();
        return $this->storeManager->getStore($orderId)->getUrl(
            'koin/callback/payments',
            [
                '_query' => ['hash' => sha1($this->getPaymentsConfig('api_key'))],
                '_secure' => true
            ]
        );
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return string
     */
    public function getAntifraudCallbackUrl($order)
    {
        $orderId = $order->getStoreId() ?: $this->storeManager->getDefaultStoreView()->getId();
        return $this->storeManager->getStore($orderId)->getUrl(
            'koin/callback/risk',
            [
                '_query' => ['hash' => sha1($this->getPaymentsConfig('api_key'))],
                '_secure' => true
            ]
        );
    }

    /**
     * @param string $type
     * @param string $incrementId
     * @return string
     */
    public function getReturnUrl($incrementId)
    {
        return $this->storeManager->getStore()->getUrl(
            'koin/success/',
            [
                '_query' => [
                    'hash' => sha1($this->getGeneralConfig('app_key')),
                    'increment_id' => $incrementId
                ],
                '_secure' => true
            ]
        );
    }

    /**
     * @param string $config
     * @return string
     */
    public function getEndpointConfig($config)
    {
        return $this->getConfig($config, 'endpoints', 'koin');
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreCurrencyCode()
    {
        return $this->storeManager->getStore()->getCurrentCurrencyCode();
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
    }

    /**
     * @return false|string
     */
    public function getCurrentIpAddress()
    {
        return $this->remoteAddress->getRemoteAddress();
    }

    /**
     * @return mixed
     */
    public function getStoreName()
    {
        return $this->getConfig('name', 'store_information', 'general');
    }

    public function getDefaultCountryCode()
    {
        return $this->helperDirectory->getDefaultCountry();
    }

    /**
     * @param int $id
     * @param null $storeId
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCategoryName($id, $storeId = null)
    {
        $categoryName = null;
        if ($id) {
            $category = $this->categoryRepository->get($id, $storeId);
            if ($category && $category->getId()) {
                $categoryName = $category->getName();
            }
        }
        return $categoryName;
    }

    /**
     * Retrieve url
     *
     * @param string $route
     * @param array $params
     * @return  string
     */
    public function getUrl($route, $params = [])
    {
        return $this->_getUrl($route, $params);
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    /**
     * @param $string
     * @return mixed
     */
    public function digits($string)
    {
        return preg_replace('/\D/', '', $string);
    }

    /**
     * @param false $customer
     * @return bool
     */
    public function isCompanyCustomer($customer = false)
    {
        $customer = $customer ?: $this->customerSession->getCustomer();
        $customerTypeAttr = $this->getConfig('customer_type_attribute', 'customer', 'koin');
        if ($customer && $customerTypeAttr) {
            $customerCompanyTypeValue = $this->getConfig('customer_type_company_value', 'customer', 'koin');
            if ($customerCompanyTypeValue) {
                $customerType = $customer->getData($customerTypeAttr);

                if ($customerType && $customerType == $customerCompanyTypeValue) {
                    return true;
                }
            }

        }

        return false;
    }

    /**
     * @param string $phoneNumber
     * @return string
     */
    public function formatPhoneNumber($phoneNumber)
    {
        return $this->clearNumber($phoneNumber);
    }

    /**
     * @param string $string
     * @return string
     */
    public function clearNumber($string)
    {
        return preg_replace('/\D/', '', $string);
    }

    /**
     * @param $incrementId
     * @return OrderInterface
     */
    public function loadOrder($incrementId)
    {
        return $this->order->loadByIncrementId($incrementId);
    }
}
