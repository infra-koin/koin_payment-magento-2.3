<?php
/**
 * Koin
 *
 * @category    Koin
 * @package     Koin_Payment
 */

namespace Koin\Payment\Helper;

use BaconQrCode\Renderer\ImageRenderer as QrCodeImageRenderer;
use BaconQrCode\Renderer\Image\ImagickImageBackEnd as QrCodeImagickImageBackEnd;
use BaconQrCode\Renderer\RendererStyle\RendererStyle as QrCodeRendererStyle;
use BaconQrCode\Writer as QrCodeWritter;
use Koin\Payment\Helper\Data as HelperData;
use Magento\Framework\App\Config\Initial;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\View\LayoutFactory;
use Magento\Payment\Model\Config;
use Magento\Payment\Model\Method\Factory;
use Magento\Sales\Model\Order\CreditmemoFactory;
use Magento\Sales\Model\Order\InvoiceRepository;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\ResourceModel\Order\Payment as ResourcePayment;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;
use Magento\Sales\Model\Service\CreditmemoService;
use Magento\Store\Model\App\Emulation;

class Order extends \Magento\Payment\Helper\Data
{
    const STATUS_APPROVED = 'approved';
    const STATUS_DENIED = 'denied';
    const DEFAULT_QRCODE_WIDTH = 400;
    const DEFAULT_QRCODE_HEIGHT = 400;
    const DEFAULT_EXPIRATION_TIME = 30;

    /**
     * @var HelperData
     */
    protected $helperData;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var OrderFactory
     */
    protected $orderRepository;

    /**
     * @var InvoiceRepository
     */
    protected $invoiceRepository;

    /**
     * @var CreditmemoFactory
     */
    protected $creditmemoFactory;

    /**
     * @var CreditmemoService
     */
    protected $creditmemoService;

    /**
     * @var ResourcePayment
     */
    protected $resourcePayment;

    /**
     * @var CollectionFactory
     */
    protected $orderStatusCollectionFactory;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * Order constructor.
     * @param Context $context
     * @param LayoutFactory $layoutFactory
     * @param Factory $paymentMethodFactory
     * @param Emulation $appEmulation
     * @param Config $paymentConfig
     * @param Initial $initialConfig
     * @param OrderFactory $orderFactory
     * @param CreditmemoFactory $creditmemoFactory
     * @param OrderRepository $orderRepository
     * @param InvoiceRepository $invoiceRepository
     * @param CreditmemoService $creditmemoService
     * @param ResourcePayment $resourcePayment
     * @param CollectionFactory $orderStatusCollectionFactory
     * @param Filesystem $filesystem
     * @param Data $helperData
     */
    public function __construct(
        Context $context,
        LayoutFactory $layoutFactory,
        Factory $paymentMethodFactory,
        Emulation $appEmulation,
        Config $paymentConfig,
        Initial $initialConfig,
        OrderFactory $orderFactory,
        CreditmemoFactory $creditmemoFactory,
        OrderRepository $orderRepository,
        InvoiceRepository $invoiceRepository,
        CreditmemoService $creditmemoService,
        ResourcePayment $resourcePayment,
        CollectionFactory $orderStatusCollectionFactory,
        Filesystem $filesystem,
        HelperData $helperData
    ) {
        parent::__construct($context, $layoutFactory, $paymentMethodFactory, $appEmulation, $paymentConfig, $initialConfig);

        $this->helperData = $helperData;
        $this->orderFactory = $orderFactory;
        $this->orderRepository = $orderRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->creditmemoFactory = $creditmemoFactory;
        $this->creditmemoService = $creditmemoService;
        $this->resourcePayment = $resourcePayment;
        $this->filesystem = $filesystem;
        $this->orderStatusCollectionFactory = $orderStatusCollectionFactory;
    }

    /**
     * Update Order Status
     *
     * @param \Magento\Sales\Model\Order $order
     * @param string $koinStatus
     * @param string $content
     * @param float $amount
     * @param bool $callback
     * @return bool
     */
    public function updateOrder($order, $koinStatus, $content, $amount, $callback = false)
    {
        try {
            /** @var \Magento\Sales\Model\Order\Payment $payment */
            $payment = $order->getPayment();
            $orderStatus = $payment->getAdditionalInformation('status');

            if ($koinStatus != $orderStatus) {
                if ($koinStatus == self::STATUS_APPROVED) {
                    if ($order->canInvoice()) {
                        $this->invoiceOrder($order, $amount);
                    }

                    $updateStatus = $order->getIsVirtual()
                        ? $this->helperData->getConfig('paid_virtual_order_status')
                        : $this->helperData->getConfig('paid_order_status');

                    $message = __('Your payment for the order %1 was confirmed', $order->getIncrementId());
                    $order->addCommentToStatusHistory($message, $updateStatus, true);
                } elseif (
                    $koinStatus == self::STATUS_DENIED
                    && $this->helperData->getConfig('cancel_unapproved_orders')
                ) {
                    $order = $this->cancelOrder($order, $amount, $callback);
                } else {
                    $order->addCommentToStatusHistory(__('Order status automatically updated to %1', __($koinStatus)));
                }
            }

            $payment = $this->updateAdditionalInfo($payment, $content);

            $this->orderRepository->save($order);
            $this->savePayment($payment);

            return true;
        } catch (\Exception $e) {
            $this->helperData->log($e->getMessage());
        }

        return false;
    }

    /**
     * @param \Magento\Sales\Model\Order\Payment $payment
     * @return void
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function savePayment($payment)
    {
        $this->resourcePayment->save($payment);
    }

    /**
     * @param \Magento\Sales\Model\Order\Payment $payment
     * @param $content
     * @return \Magento\Sales\Model\Order\Payment|mixed
     */
    protected function updateAdditionalInfo($payment, $content)
    {
        if ($payment->getMethod() == \Koin\Payment\Model\Ui\Pix\ConfigProvider::CODE) {
            $payment = $this->updatePixAdditionalInfo($payment, $content);
        } elseif ($payment->getMethod() == \Koin\Payment\Model\Ui\Redirect\ConfigProvider::CODE) {
            $payment = $this->updateRedirectAdditionalInfo($payment, $content);
        }

        return $payment;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param float $amount
     */
    protected function invoiceOrder($order, $amount)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();
        $payment->setParentTransactionId($payment->getLastTransId());
        $payment->registerCaptureNotification($amount);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param $content
     * @return \Magento\Sales\Model\Order
     */
    public function updateInterestRate($order, $content)
    {
        if (isset($content['amount']) && isset($content['amount']['value'])) {
            $amountValue = (float)$content['amount']['value'];
            if ($amountValue > $order->getBaseGrandTotal()) {
                $currency = $content['amount']['currency'] ?: $order->getBaseCurrencyCode();
                if ($order->getBaseCurrencyCode() == $currency) {
                    $interest = $amountValue - $order->getBaseGrandTotal();
                    $order->setData('koin_interest_amount', $interest);
                    $order->setData('base_koin_interest_amount', $interest);
                    $order->setGrandTotal($amountValue);
                    $order->setBaseGrandTotal($amountValue);
                }
            }
        }

        return $order;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param float $amount
     * @param boolean $callback
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Magento\Sales\Model\Order $order
     */
    public function cancelOrder($order, $amount, $callback = false)
    {
        if ($order->canCreditmemo() && ($callback || $this->helperData->getGeneralConfig('refund_on_cancel'))) {
            $creditMemo = $this->creditmemoFactory->createByOrder($order);
            $this->creditmemoService->refund($creditMemo, true);
        } elseif ($order->canCancel()) {
            $order->cancel();
        }

        $cancelledStatus = $this->helperData->getConfig('cancelled_order_status') ?: false;
        $order->addCommentToStatusHistory(__('The order %1 was cancelled. Amount of %2'), $cancelledStatus, $amount);

        return $order;
    }

    /**
     * @param $order
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function credimemoOrder($order)
    {
        $creditMemo = $this->creditmemoFactory->createByOrder($order);
        $this->creditmemoService->refund($creditMemo);
    }

    /**
     * @param \Magento\Sales\Model\Order\Payment $payment
     * @return \Magento\Sales\Model\Order\Payment
     */
    public function updateRedirectAdditionalInfo($payment, $content)
    {
        try {
            if (isset($content['tid'])) {
                $payment->setTransactionId($content['tid']);
                $payment->setLastTransId($content['tid']);
                $payment->setAdditionalInformation('tid', $content['tid']);
            }

            if (isset($content['id'])) {
                $payment->setAdditionalInformation('id', $content['id']);
                if (!isset($content['tid'])) {
                    $payment->setTransactionId($content['id']);
                    $payment->setLastTransId($content['id']);
                }
            }

            if (isset($content['status'])) {
                $payment->setAdditionalInformation('status', $content['status']);
            }

            if (isset($content['status_reason'])) {
                $payment->setAdditionalInformation('status_reason', $content['status_reason']);
            }

            if (isset($content['amount'])) {
                if (isset($content['amount']['value'])) {
                    $payment->setAdditionalInformation('total_amount', $content['amount']['value']);
                }

                if (isset($content['amount']['currency'])) {
                    $payment->setAdditionalInformation('currency', $content['amount']['currency']);
                }
            }

            if (isset($content['paymentUrl'])) {
                $payment->setAdditionalInformation('payment_url', $content['paymentUrl']);
            }

            if (isset($content['authorizationId'])) {
                $payment->setAdditionalInformation('authorization_id', $content['authorizationId']);
            }

            if (isset($content['installments'])) {
                $payment->setAdditionalInformation('installments', $content['installments']);
            }

            if (isset($content['paymentId'])) {
                $payment->setAdditionalInformation('koin_payment_id', $content['paymentId']);
            }

            if (isset($content['delayToAutoSettle'])) {
                $payment->setAdditionalInformation('delay_to_auto_settle', $content['delayToAutoSettle']);
            }

            if (isset($content['delayToAutoSettleAfterAntifraud'])) {
                $payment->setAdditionalInformation(
                    'delay_to_auto_settle_after_antifraud',
                    $content['delayToAutoSettleAfterAntifraud']
                );
            }

            if (isset($content['delayToCancel'])) {
                $payment->setAdditionalInformation('delay_to_cancel', $content['delayToCancel']);
            }
        } catch (\Exception $e) {
            $this->_logger->warning($e->getMessage());
        }

        return $payment;
    }

    /**
     * @param \Magento\Sales\Model\Order\Payment $payment
     * @return \Magento\Sales\Model\Order\Payment
     */
    public function updatePixAdditionalInfo($payment, $content)
    {
        try {

            if (isset($content['order_id'])) {
                $payment->setTransactionId($content['order_id']);
                $payment->setLastTransId($content['order_id']);
                $payment->setAdditionalInformation('order_id', $content['order_id']);
            }

            if (isset($content['transaction']['reference_id'])) {
                $payment->setAdditionalInformation('reference_id', $content['transaction']['reference_id']);
            }

            if (isset($content['transaction']['business_id'])) {
                $payment->setAdditionalInformation('business_id', $content['transaction']['business_id']);
            }

            if (isset($content['transaction']['account'])) {
                $payment->setAdditionalInformation('status', $content['transaction']['account']);
            }

            if (isset($content['transaction']['amount'])) {
                if (isset($content['transaction']['amount']['value'])) {
                    $payment->setAdditionalInformation('total_amount', $content['transaction']['amount']['value']);
                }

                if (isset($content['transaction']['amount']['currency_code'])) {
                    $payment->setAdditionalInformation(
                        'currency_code',
                        $content['transaction']['amount']['currency_code']
                    );
                }
            }

            //Set an order status, if it doesn't have the status in response, set the Published status
            $status = \Koin\Payment\Gateway\Http\Client\Payments\Api::STATUS_PUBLISHED;
            if (isset($content['status'])) {
                if (isset($content['status']['type'])) {
                    $status = $content['status']['type'];
                }
                if (isset($content['status']['date'])) {
                    $payment->setAdditionalInformation('status_date', $content['status']['date']);
                }
            }
            $payment->setAdditionalInformation('status', $status);

            if (isset($content['locations'])) {
                if (isset($content['locations'][0])) {
                    $location = $content['locations'][0];

                    if (isset($location['location']['qr_code'])) {
                        $payment->setAdditionalInformation('qr_code', $location['location']['qr_code']);
                    }

                    if (isset($location['location']['emv'])) {
                        $payment->setAdditionalInformation('qr_code_emv', $location['location']['emv']);
                        if (isset($location['location']['url'])) {
                            $qrCodeUrl = $location['location']['url'];
                        } else {
                            $qrCodeUrl = $this->generateQrCode($payment, $location['location']['emv']);
                        }
                        $payment->setAdditionalInformation('qr_code_url', $qrCodeUrl);
                    }
                }
            }

            $payment->setIsTransactionClosed(false);
        } catch (\Exception $e) {
            $this->_logger->warning($e->getMessage());
        }

        return $payment;
    }

    /**
     * @param $payment
     * @param $qrCode
     * @return string|null
     */
    public function generateQrCode($payment, $qrCode)
    {
        $pixUrl = null;
        if ($qrCode) {
            try {
                $renderer = new QrCodeImageRenderer(
                    new QrCodeRendererStyle(self::DEFAULT_QRCODE_WIDTH),
                    new QrCodeImagickImageBackEnd()
                );
                $writer = new QrCodeWritter($renderer);
                $pixQrCode = $writer->writeString($qrCode);

                $filename = 'koin/pix-' . $payment->getOrder()->getIncrementId() . '.png';
                $media = $this->filesystem->getDirectoryWrite(DirectoryList::MEDIA);
                $media->writeFile($filename, $pixQrCode);

                $pixUrl = $this->helperData->getMediaUrl() . $filename;
            } catch (\Exception $e) {
                $this->helperData->log($e->getMessage());
            }
        }

        return $pixUrl;
    }

    /**
     * @param string $incrementId
     * @return \Magento\Sales\Model\Order
     */
    public function loadOrder($incrementId)
    {
        $order = $this->orderFactory->create();
        if ($incrementId) {
            $order->loadByIncrementId($incrementId);
        }

        return $order;
    }

    /**
     * Returns a state code from a status code
     *
     * @param $status
     * @return string
     */
    public function getStatusState($status)
    {
        if ($status) {
            $statuses = $this->orderStatusCollectionFactory
                ->create()
                ->joinStates()
                ->addFieldToFilter('main_table.status', $status);

            if ($statuses->getSize()) {
                return $statuses->getFirstItem()->getState();
            }
        }

        return '';
    }

    /**
     * @param $payment
     * @return false|string
     */
    public function getPaymentStatusState($payment)
    {
        $paymentMethod = $payment->getMethodInstance();
        if (!$paymentMethod) {
            return false;
        }

        $status = $paymentMethod->getConfigData('order_status');
        if (!$status) {
            return false;
        }

        $state = $this->getStatusState($status);
        if (!$state) {
            return false;
        }

        return $state;
    }

    /**
     * @param $state
     * @return bool
     */
    public function canSkipOrderProcessing($state)
    {
        return $state != \Magento\Sales\Model\Order::STATE_PROCESSING;
    }
}
