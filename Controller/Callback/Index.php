<?php
/**
 *
 *
 *
 * @category    Koin
 * @package     Koin_Payment
 */

namespace Koin\Payment\Controller\Callback;

use Magento\Framework\Controller\ResultFactory;
use Koin\Payment\Controller\Callback;

class Index extends Callback
{
    /**
     * @var string
     */
    protected $eventName = 'redirect';

    /**
        {
            "status": "undefined" | "approved" | "denied",
            "status_reason": "EmailValidation" | "ProviderReview" | "FirstPayment",
            "amount": {
                "currency": "BRL",
                "value": 100.00
            },
            "paymentId": "1B1DE08F39B447ED8E36520B33B56580",
            "paymentUrl": "https://payments.koin.com.br/checkout/4f83bbd8-be3d-4543-9f71-d5f5244758b4"
            "authorizationId": "2738",
            "installments": 6
        }
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->helperData->log(__('Webhook %1', __CLASS__), self::LOG_NAME);

        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $statusCode = 500;

        try {
            $content = $this->getContent($this->getRequest());

            $params = $this->getRequest()->getParams();
            $this->logParams($content, $params);

            $order = $this->helperOrder->loadOrder($content['paymentId']);
            if ($order && $order->getId()) {
                $amount = $this->getCallbackAmount($order, $content);
                $order = $this->helperOrder->updateInterestRate($order, $content);
                $this->helperOrder->updateOrder($order, $content['status'], $content, $amount, true);
                $statusCode = 204;
            }

            /** @var \Koin\Payment\Model\Callback $callBack */
            $callBack = $this->callbackFactory->create();
            $callBack->setStatus($content['status']);
            $callBack->setMethod(\Koin\Payment\Model\Ui\Redirect\ConfigProvider::CODE);
            $callBack->setIncrementId($content['paymentId']);
            $callBack->setPayload($this->json->serialize($content));
            $this->callbackResourceModel->save($callBack);

        } catch (\Exception $e) {
            $this->helperData->getLogger()->error($e->getMessage());
        }

        $result->setHttpResponseCode($statusCode);
        return $result;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param array $content
     * @return string
     */
    protected function getCallbackAmount($order, $content)
    {
        //Update Interest Rate
        $amount = $order->getBaseGrandTotal();
        if (
            isset($content['amount'])
            && isset($content['amount']['currency'])
            && isset($content['amount']['value'])
        ) {
            $amount = $content['amount']['value'];
        }

        return $amount;
    }
}
