<?php
/**
 *
 *
 *
 * @category    Koin
 * @package     Koin_Payment
 */

namespace Koin\Payment\Controller\Callback;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Koin\Payment\Controller\Callback;
use Koin\Payment\Gateway\Http\Client\Payments\Api;
use Koin\Payment\Helper\Order;

class Payments extends Callback
{
    /**
     * @var string
     */
    protected $eventName = 'pix';

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        $hash = $request->getParam('hash');
        $storeHash = sha1($this->helperData->getPaymentsConfig('api_key'));
        return ($hash == $storeHash);
    }

    /**
     * Exemplo:
    {
        "status": {
            "type": "Collected", //Published, Collected, Refunded, Cancelled, Voided, Failed.
            "date": "2021-09-03T15:27:28.000Z"
        },
        "transaction": {
            "reference_id": "FGCCCEA53JYSHXX",
            "business_id": "6LVHR8W0V7BYEQX",
            "account": "Merchant1234",
            "amount": {
                "currency_code":"BRL",
                "value":1111.11
            }
        }
        "order_id": "7978c0c97ea847e78e8849634473c1f1",
        "refund_id":"9943a9f6-b6fc-4a2b-b004-8cd484e08cdd",
        "refund_amount":{
            "currency_code":"BRL",
            "value":500.0
        }
    }
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $this->helperData->log(__('Webhook %1', __CLASS__), self::LOG_NAME);

        $result = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $statusCode = 500;

        try {
            $content = $this->getContent($this->getRequest());
            $params = $this->getRequest()->getParams();
            $this->logParams($content, $params);

            if (isset($content['transaction'])) {
                $transaction = $content['transaction'];
                if (isset($content['status'])) {

                    $statusLabel = $content['status']['type'] ?? $content['status'];
                    $status = $this->getStatus($statusLabel);

                    $order = $this->helperOrder->loadOrder($transaction['reference_id']);
                    if ($order && $order->getId()) {
                        $amount = $this->getCallbackAmount($order, $content);
                        $this->helperOrder->updateOrder($order, $status, $content, $amount,true);
                        $statusCode = 204;
                    } else if ($statusLabel == Api::STATUS_FAILED) {
                        $statusCode = 204;
                    }

                    /** @var \Koin\Payment\Model\Callback $callBack */
                    $callBack = $this->callbackFactory->create();
                    $callBack->setStatus($statusLabel);
                    $callBack->setMethod(\Koin\Payment\Model\Ui\Pix\ConfigProvider::CODE);
                    $callBack->setIncrementId($transaction['reference_id']);
                    $callBack->setPayload($this->json->serialize($content));
                    $this->callbackResourceModel->save($callBack);
                }
            }

        } catch (\Exception $e) {
            $statusCode = 500;
            $this->helperData->getLogger()->error($e->getMessage());
        }

        $result->setHttpResponseCode($statusCode);
        return $result;
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param array $content
     * @return string
     */
    protected function getCallbackAmount($order, $content)
    {
        //Update Interest Rate
        $amount = $order->getBaseGrandTotal();
        if (isset($content['amount']) && isset($content['amount']['value']) ) {
            $amount = $content['amount']['value'];
        }

        //If there's refund amount
        if (isset($content['refund_amount']) && isset($content['refund_amount']['value']) ) {
            $amount = $content['refund_amount']['value'];
        }

        return $amount;
    }

    /**
     * @param $callbackStatus
     * @return string
     */
    protected function getStatus($callbackStatus)
    {
        switch ($callbackStatus) {
            case Api::STATUS_COLLECTED:
                $status = Order::STATUS_APPROVED;
                break;

            case Api::STATUS_REFUNDED;
            case Api::STATUS_CANCELLED;
            case Api::STATUS_FAILED;
            case Api::STATUS_VOIDED;
                $status = Order::STATUS_DENIED;
                break;

            default:
                $status = $callbackStatus;
        }

        return $status;
    }
}
