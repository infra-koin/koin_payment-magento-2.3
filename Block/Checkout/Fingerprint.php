<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Block\Checkout;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\View\Element\Template;
use Koin\Payment\Helper\Data;

class Fingerprint extends Template
{
    /**
     * @var CustomerSession $customerSession
     */
    protected $customerSession;

    /**
     * @var Data $helper
     */
    protected $helper;

    /**
     * @param Template\Context $context
     * @param CustomerSession $customerSession
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CustomerSession $customerSession,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->customerSession = $customerSession;
        $this->helper = $helper;
    }

    public function getFingerprintId()
    {
        return $this->customerSession->getSessionId();
    }

    public function getFingerprintUrl()
    {
        return Data::FINGERPRINT_URL;
    }

    public function getOrgId()
    {
        return trim($this->helper->getGeneralConfig('org_id'));
    }
}
