<?php
/**
 *
 *
 *
 *
 *
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Koin
 * @package     Koin_Payment
 *
 *
 */

namespace Koin\Payment\Block\Info;

use Koin\Payment\Gateway\Http\Client\Redirect\Api;
use Magento\Framework\Phrase;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Element\Template\Context;
use Magento\Payment\Gateway\ConfigInterface;
use Magento\Payment\Model\Config;

class Redirect extends AbstractInfo
{
    protected $_template = 'Koin_Payment::payment/info/redirect.phtml';

    /**
     * @var ConfigInterface
     */
    protected $config;

    /**
     * @var Config
     */
    protected $paymentConfig;

    /**
     * @var  DateTime
     */
    protected $date;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * BankSlip constructor.
     * @param Context $context
     * @param ConfigInterface $config
     * @param Config $paymentConfig
     * @param PriceCurrencyInterface $priceCurrency
     * @param DateTime $date
     * @param array $data
     */
    public function __construct(
        Context $context,
        ConfigInterface $config,
        Config $paymentConfig,
        PriceCurrencyInterface $priceCurrency,
        DateTime $date,
        array $data = []
    ) {
        parent::__construct($context, $config, $paymentConfig, $data);
        $this->paymentConfig = $paymentConfig;
        $this->date = $date;
        $this->priceCurrency = $priceCurrency;
    }

    /**
     * @return Phrase
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getTitle()
    {
        $title = __('Seu pedido de crédito está sendo processado!');
        $payment = $this->getInfo();
        $status = $payment->getAdditionalInformation('status');
        if ($status == 'denied') {
            $title = __('Seu pedido foi cancelado!');
        } else if ($status == 'approved') {
            $title = __('Seu pedido foi confirmado com sucesso!');
        }

        return $title;
    }

    /**
     * @return Phrase
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getDescription()
    {
        $description = __('Koin will send you an email with the next steps about your request. Be ready :)');
        $payment = $this->getInfo();
        $status = $payment->getAdditionalInformation('status');
        if ($status == Api::STATUS_DENIED) {
            $description = __('Your request with Koin wasn\'t approved right now, for that reason your order was cancelled');
        } else if ($status == Api::STATUS_APPROVED) {
            $description = __('Everything ok with your order, we identified your first payment and we\'ll proceed with your purchase!');
            $description .= '<br>';
            $description .= __('We\'ll send you a confirmation email with informations about your account.');
        }

        return $description;
    }

    /**
     * @return Phrase
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getReason()
    {
        $reason = '';
        $payment = $this->getInfo();
        $paymentModel = $payment->getMethodInstance();
        $status = $payment->getAdditionalInformation('status');
        if ($status == 'undefined') {
            $statusReason = $payment->getAdditionalInformation('status_reason');
            if ($statusReason) {
                switch ($statusReason) {
                    case API::STATUS_REASON_EMAIL_VALIDATION:
                        $reason = $paymentModel->getConfigData('text_email_validation');
                        break;
                    case API::STATUS_REASON_PROVIDER_REVIEW:
                        $reason = $paymentModel->getConfigData('text_provider_review');
                        break;
                    case API::STATUS_REASON_FIRST_PAYMENT:
                        $reason = $paymentModel->getConfigData('text_waiting_first_payment');
                        break;

                }
            }
        }

        return $reason;
    }

}
